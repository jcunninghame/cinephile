/* Populate tables */
INSERT INTO Movie(title, year, director) values('Gone with the Wind', 1939, 'Victor Fleming');
INSERT INTO Movie(title, year, director) values('Star Wars', 1977, 'George Lucas');
INSERT INTO Movie(title, year, director) values('The Sound of Music', 1965, 'Robert Wise');
INSERT INTO Movie(title, year, director) values('E.T.', 1982, 'Steven Spielberg');
INSERT INTO Movie(title, year, director) values('Titanic', 1997, 'James Cameron');
INSERT INTO Movie(title, year, director) values('Snow White', 1937, null);
INSERT INTO Movie(title, year, director) values('Avatar', 2009, 'James Cameron');
INSERT INTO Movie(title, year, director) values('Raiders of the Lost Ark', 1981, 'Steven Spielberg');

