/* Delete the tables if they already exist */
DROP TABLE if EXISTS Movie;

/* Create the schema for tables */
CREATE TABLE Movie (mid INTEGER IDENTITY PRIMARY KEY, title VARCHAR(255), year INTEGER, director VARCHAR(255));

