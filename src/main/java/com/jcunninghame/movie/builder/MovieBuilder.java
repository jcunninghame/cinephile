package com.jcunninghame.movie.builder;

import com.jcunninghame.movie.Movie;

public class MovieBuilder {

    private Long mid;
    private String title;
    private int year;
    private String director;

    public MovieBuilder() { }

    public Movie build() {
        return new Movie(this.title, this.year, this.director);
    }

    public MovieBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public MovieBuilder withYear(int year) {
        this.year = year;
        return this;
    }

    public MovieBuilder withDirector(String director) {
        this.director = director;
        return this;
    }
}
