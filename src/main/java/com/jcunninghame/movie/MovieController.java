package com.jcunninghame.movie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Properties;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @RequestMapping(value="{title}", method = GET)
    public ModelAndView getMovie(@PathVariable(value="title") String title) {
        Movie movie = movieService.getMovie(title);
        return new ModelAndView("movie/movie", "movie", movie);
    }

    @RequestMapping(method = GET)
    public ModelAndView getMovies() {
        List<Movie> movies = movieService.getMovies();
        return new ModelAndView("index", "movies", movies);
    }

    @RequestMapping("properties")
    @ResponseBody
    public Properties properties() {
        return System.getProperties();
    }
}
