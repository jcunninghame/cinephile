package com.jcunninghame.movie;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MovieService {

    Movie getMovie(String title);

    void saveMovie(Movie movie);

    List<Movie> getMovies();
}
