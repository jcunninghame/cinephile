package com.jcunninghame.config;

import com.jcunninghame.movie.MovieService;
import com.jcunninghame.movie.MovieServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan( value = { "com.jcunninghame" },
        excludeFilters = @ComponentScan.Filter(Configuration.class))
@Import(value = {
        WebMvcConfiguration.class,
        DatabaseConfiguration.class
})
public class ApplicationConfiguration {

    @Bean
    public MovieService movieService() { return new MovieServiceImpl(); }

}
