package com.jcunninghame.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( { "com.jcunninghame" } )
public class TestConfiguration extends ApplicationConfiguration {

}
