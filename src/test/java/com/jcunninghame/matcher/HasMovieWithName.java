package com.jcunninghame.matcher;

import com.jcunninghame.movie.Movie;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.util.List;
import java.util.stream.Collectors;

public class HasMovieWithName extends BaseMatcher<List<Movie>> {

    private final String movieName;

    public HasMovieWithName(final String movieName) {
        this.movieName = movieName;
    }

    public static HasMovieWithName hasMovieWithName(final String movieName) {
        return new HasMovieWithName(movieName);
    }

    @Override
    public boolean matches(Object o) {
        final List<Movie> movies = (List<Movie>) o;
        List<String> movieNames =  movies.stream().map(Movie::getTitle).collect(Collectors.toList());
        return movieNames.contains(movieName);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("movie collection does not contain ").appendValue(movieName);
    }
}