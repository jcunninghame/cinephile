package com.jcunninghame.webtest;

import com.jcunninghame.movie.Movie;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

public class MoviesPage {

    private final WebDriver driver;

    @FindBy(xpath = "//*[@id='navi']/ul/li")
    private List<WebElement> actuatorLinks;

    @FindBy(id = "movies")
    private List<WebElement> movies;

    public MoviesPage(WebDriver driver) {
        this.driver = driver;
    }

    public MovieDetailPage selectMovie(String string) {
        driver.findElement(By.linkText(string)).click();
        return PageFactory.initElements(driver, MovieDetailPage.class);
    }

    public List<Movie> getMovies() {
        return movies.stream()
                .map(row ->
                        new Movie(
                                getCellFromRow(row, 1).getText(),
                                parseInt(getCellFromRow(row, 2).getText()),
                                getCellFromRow(row, 3).getText()
                        )
                )
                .collect(toList());
    }

    private WebElement getCellFromRow(WebElement row, int cellNum) {
        return row.findElement(By.xpath(format("td[%d]", cellNum)));
    }

    public List<String> getActuatorLinks() {
        return actuatorLinks.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public String getPageTitle() {
        return driver.getTitle();
    }
}
