package com.jcunninghame.webtest.mockmvc;

import com.jcunninghame.movie.Movie;
import com.jcunninghame.webtest.IntegrationWebTestBase;
import com.jcunninghame.webtest.MovieDetailPage;
import com.jcunninghame.webtest.MoviesPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import selenium.MockMvcHtmlUnitTest;

import java.util.List;

import static com.jcunninghame.matcher.HasMovieWithName.hasMovieWithName;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

@MockMvcHtmlUnitTest(baseUrl = "http://localhost:9000")
public class MovieMockMvcWebTest extends IntegrationWebTestBase {

    private MoviesPage moviesPage;

    @Before
    public void setUpPage() {
        moviesPage = PageFactory.initElements(driver, MoviesPage.class);
    }

    @Test
    public void shouldShowListOfMovies() {
        List<Movie> movies = moviesPage.getMovies();
        assertThat(movies.size(), is(8));
    }

    @Test
    public void shouldHaveLinkToAvatar() {
        List<Movie> movies = moviesPage.getMovies();
        assertThat(movies, hasMovieWithName("Avatar"));
    }

    @Test
    public void shouldLinkToMovie() {
        MovieDetailPage movieDetailPage = moviesPage.selectMovie("Avatar");
        assertThat(movieDetailPage.getTitle(), is("Avatar"));
        assertThat(movieDetailPage.getDirector(), is("James Cameron"));
        assertThat(movieDetailPage.getYear(), is(2009));
    }

    @Test
    public void shouldBeAbleToClickBackHomeAfterLinkingToMovie() {
        MovieDetailPage movieDetailPage = moviesPage.selectMovie("Avatar");
        assertThat(movieDetailPage.getPageTitle(), is("Cinephile :: Movie Details"));

        moviesPage = movieDetailPage.returnToHome();
        assertThat(moviesPage.getPageTitle(), is("Cinephile :: Movies"));
    }

    @Test
    public void shouldContainActuatorLinks() {
        assertThat(moviesPage.getActuatorLinks(),
                hasItems("AutoConfig", "Beans", "ConfigProps", "Dump", "ConfigEnv", "Health", "Info", "Metrics", "Mappings", "Trace"));
    }
}
