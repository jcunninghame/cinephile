package com.jcunninghame.webtest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MovieDetailPage {

    @FindBy(id = "title")
    private WebElement title;

    @FindBy(id = "director")
    private WebElement director;

    @FindBy(id = "year")
    private WebElement year;

    private final WebDriver driver;

    public MovieDetailPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitle() {
        return title.getText();
    }

    public String getDirector() {
        return director.getText();
    }

    public int getYear() {
        return Integer.parseInt(year.getText());
    }


    public MoviesPage returnToHome() {
        driver.findElement(By.linkText("Home")).click();
        return PageFactory.initElements(driver, MoviesPage.class);
    }

    public String getPageTitle() {
        return driver.getTitle();
    }
}
