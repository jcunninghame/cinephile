package com.jcunninghame.webtest;

import com.jcunninghame.integration.IntegrationTestBase;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.WebIntegrationTest;

@WebIntegrationTest(value = "server.port:9000")
public class IntegrationWebTestBase extends IntegrationTestBase {

    @Value("${local.server.port}")
    protected int serverPort;

    @Autowired
    protected WebDriver driver;

    protected String getBaseUrl() {
        return "http://localhost:" + serverPort;
    }

}

