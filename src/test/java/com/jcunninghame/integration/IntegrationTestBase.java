package com.jcunninghame.integration;

import com.jcunninghame.config.TestConfiguration;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration( classes = { TestConfiguration.class } )
@Transactional
public class IntegrationTestBase {
}
