package com.jcunninghame.integration;

import com.jcunninghame.movie.Movie;
import com.jcunninghame.movie.MovieService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@WebAppConfiguration
public class MovieServiceTest extends IntegrationTestBase {

    @Autowired
    private MovieService movieService;

    @Test
    public void shouldSaveMovie() {
        Movie newMovie = MovieFactory.blankMovie()
                .withTitle("New Movie")
                .withDirector("The Director")
                .withYear(2000)
                .build();

        movieService.saveMovie(newMovie);
        Movie movie = movieService.getMovie("New Movie");

        assertThat(movie.getTitle(), is("New Movie"));
        assertThat(movie.getDirector(), is("The Director"));
        assertThat(movie.getYear(), is(2000));
    }
}