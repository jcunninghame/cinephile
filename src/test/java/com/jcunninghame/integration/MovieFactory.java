package com.jcunninghame.integration;

import com.jcunninghame.movie.builder.MovieBuilder;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

public class MovieFactory {

    public static MovieBuilder randomMovie() {
        return new MovieBuilder()
                .withTitle(RandomStringUtils.random(7))
                .withDirector(RandomStringUtils.random(11))
                .withYear(RandomUtils.nextInt(1920, 2016));
    }

    public static MovieBuilder blankMovie() {
        return new MovieBuilder()
                .withTitle("")
                .withDirector("")
                .withYear(0);
    }
}
